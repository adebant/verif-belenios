(*
Time = 3s
 *)

set ignoreTypes = false. (* Needed to take 'nat' into account *)
set verboseRules = false.
set preciseActions = false.
set reconstructTrace = true.

free target_reject:bitstring [private].
free target_verif:bitstring [private].
free initEmpty:bitstring.

type stamp.

(* Public channel used for communications *)
free cp:channel.

(* Encryption scheme *)
fun pubeThreshold(bitstring,bitstring,bitstring) : bitstring. (* Symbol used to derive the public encryption key correponding to several private encryption keys *)
fun pubeMandatory(bitstring) : bitstring.
fun pube(bitstring, bitstring) : bitstring. (* One mandatory + one for the  threshold scheme *)
fun aenc(bitstring,bitstring,bitstring) : bitstring. (* Encryption *)
reduc forall M:bitstring, SkM:bitstring, Sk1:bitstring, R1:bitstring, Sk2:bitstring, Sk3:bitstring;  (* MAGIC - Full decryption *)
  adec(SkM,Sk1,Sk2,Sk3,aenc(pube(pubeMandatory(SkM),pubeThreshold(Sk1,Sk2,Sk3)),M,R1)) = M.
fun partADec(bitstring,bitstring) : bitstring.
fun f(bitstring,bitstring,bitstring) : bitstring
  reduc forall M:bitstring, SkM:bitstring, Sk1:bitstring, R1:bitstring, Sk2:bitstring, Sk3:bitstring;  (* Partial decryption *)
    f(
      partADec(SkM,aenc(pube(pubeMandatory(SkM),pubeThreshold(Sk1,Sk2,Sk3)),M,R1)),
      partADec(Sk1,aenc(pube(pubeMandatory(SkM),pubeThreshold(Sk1,Sk2,Sk3)),M,R1)),
      partADec(Sk2,aenc(pube(pubeMandatory(SkM),pubeThreshold(Sk1,Sk2,Sk3)),M,R1))
      ) = M
  otherwise forall M:bitstring, SkM:bitstring, Sk1:bitstring, R1:bitstring, Sk2:bitstring, Sk3:bitstring;  (* Partial decryption *)
    f(
      partADec(SkM,aenc(pube(pubeMandatory(SkM),pubeThreshold(Sk1,Sk2,Sk3)),M,R1)),
      partADec(Sk1,aenc(pube(pubeMandatory(SkM),pubeThreshold(Sk1,Sk2,Sk3)),M,R1)),
      partADec(Sk3,aenc(pube(pubeMandatory(SkM),pubeThreshold(Sk1,Sk2,Sk3)),M,R1))
      ) = M
  otherwise forall M:bitstring, SkM:bitstring, Sk1:bitstring, R1:bitstring, Sk2:bitstring, Sk3:bitstring;  (* Partial decryption *)
    f(
      partADec(SkM,aenc(pube(pubeMandatory(SkM),pubeThreshold(Sk1,Sk2,Sk3)),M,R1)),
      partADec(Sk2,aenc(pube(pubeMandatory(SkM),pubeThreshold(Sk1,Sk2,Sk3)),M,R1)),
      partADec(Sk3,aenc(pube(pubeMandatory(SkM),pubeThreshold(Sk1,Sk2,Sk3)),M,R1))
      ) = M.


fun pubs(bitstring):bitstring.
fun privs(bitstring,bitstring) : bitstring.
fun Sign(bitstring,bitstring):bitstring.

reduc forall Sk:bitstring,M:bitstring; Verify(pubs(Sk),M,Sign(Sk,M)) = true.

fun seed(bitstring,bitstring,bitstring):bitstring [private].
reduc forall uuid,id,sd:bitstring; getSeed(uuid,id,seed(uuid,id,sd)) = sd [private].
fun password(bitstring,bitstring,bitstring):bitstring [private].
reduc forall uuid,id,pw:bitstring; getPassword(uuid,id,password(uuid,id,pw)) = pw [private].
fun SignAUTH(bitstring,bitstring):bitstring.

reduc forall Sk:bitstring,M:bitstring; VerifyAUTH(pubs(Sk),M,SignAUTH(Sk,M)) = true.

const ZERO:bitstring.
const ONE:bitstring.

fun proofv(bitstring,bitstring,bitstring,bitstring,bitstring,bitstring):bitstring.
fun verifproofv(bitstring,bitstring,bitstring,bitstring,bitstring):bool
reduc forall r:bitstring, pk:bitstring, vk:bitstring, uuid:bitstring;
    verifproofv(proofv(ZERO,r,aenc(pk,ZERO,r),pk,vk,uuid),aenc(pk,ZERO,r),pk,vk,uuid) = true
  otherwise forall r:bitstring, pk:bitstring, vk:bitstring, uuid:bitstring;
      verifproofv(proofv(ONE,r,aenc(pk,ONE,r),pk,vk,uuid),aenc(pk,ONE,r),pk,vk,uuid) = true.


fun cellPK(bitstring):channel [private].
const cellPKt:channel [private].
fun cellPKvs(bitstring):channel [private].

fun cellVK(bitstring):channel [private].
fun cellB(bitstring,bitstring):channel [private].
(* The channel cellB(uuid,vk) contains ballots of the form (j,ballot) where:
- j is the current counter correponding to the verification key vk
- ballot is the ballot
*)

(* Constants and events used to register a voter *)
const CORRUPT:bitstring.
event Corrupted(bitstring,bitstring,bitstring).
event Registered(bitstring,bitstring,bitstring).
fun OkReg(bitstring,bitstring):bitstring [private]. (* OkReg(uuid,vk) - The Registrar has verified that 'vk' is a credent it has generated for election 'uuid' *)

(* Events used to model the voter's behaviors *)
event Voted(bitstring,bitstring,bitstring,bitstring). (* Voted(uuid,id,vk,v) means that Voter 'id' with credential 'vk' voted for 'v' in election 'uuid' *)
const IsVerified:bitstring. (* Constant used to decide whether ballot is verified by the Voter *)
event VerifVoter(bitstring,bitstring,bitstring,bitstring). (* VerifVoter(uuid,id,vk,b,i,j) means that Voter 'id' with credential 'vk' verified her ballot 'b' containing counter 'j' at time 'i' (cellB - modelling) in election 'uuid' *)

(* Events used to model the Server's behaviors *)
event Accepted(bitstring,bitstring,bitstring,bitstring,nat). (* Accepted(uuid,id,vk,b,i,j) means that the ballot 'b' cast by voter 'id' with credential 'vk' and containing the counter 'j' has been accepted at time 'i' (cellB - modelling) in election 'uuid' *)
event Rejected(bitstring,bitstring,bitstring,bitstring,nat). (* Rejected(uuid,id,vk,b,i,j) means that the ballot 'b' cast by voter 'id' with credential 'vk' and containing the counter 'j' has been rejected at time 'i' (cellB - modelling) in election 'uuid' *)
event In_log(bitstring,bitstring,bitstring). (* In_log(uuid,id,vk) - in election 'uuid', the Server has accepted a ballot to voter 'id' using the credential 'vk' *)
restriction uuid:bitstring,xLock,xLock':nat,id,id',vk,vk':bitstring;
  event(In_log(uuid,id,vk)) && event(In_log(uuid,id',vk')) ==> (id = id' && vk = vk') || (id <> id' && vk <> vk').

(* Events used to model the Trustee and the Auditor behaviors *)
fun OkTrustee(bitstring):bitstring [private]. (* OkTrustee(pkT) - The (threshold) Trustees have verified that their election public key have been published by the Server *)
fun OkPK(bitstring,bitstring):bitstring [private]. (* OkPK(pk) - An auditor has verified that pk = pube(pkVS,pkT), i.e. is the valid public key for election 'uuid' *)


(* Registration of 'voter_id' for the election 'uuid' using 'seed' to derive her credential *)
let Registrar(uuid:bitstring) =
  in(cp, voter_id:bitstring);
  in(cp,x_voter_seed:bitstring); let voter_seed = getSeed(uuid,voter_id,x_voter_seed) in
  let sk = privs(voter_seed,uuid) in
  out(cp,pubs(sk));

  (* checks of correct initialisation by the Server *)
  in(cellVK(uuid), =pubs(sk));
  out(cp,OkReg(uuid,pubs(sk)));
  0.

(* Corruption of a voter *)
let May_corrupt_voter(voter_id:bitstring, seed:bitstring, password:bitstring, uuid:bitstring) =
  in(cp, =CORRUPT);
  let sk = privs(seed,uuid) in
  event Corrupted(uuid,voter_id,pubs(sk));
  out(cp,password);
  out(cp,sk);
  0.

let Voter_Device(Voter:bitstring, v:bitstring, uuid:bitstring, personnal_data:bitstring) =
  let (x_voter_password:bitstring,x_voter_seed:bitstring) = personnal_data in
  let voter_password = getPassword(uuid,Voter,x_voter_password) in (* receive Voter's password *)
  let voter_seed = getSeed(uuid,Voter,x_voter_seed) in (* receive Voter's credential *)
  let sk_id = privs(voter_seed,uuid) in

  in(cellPK(uuid),pk:bitstring);
  in(cp,= OkPK(uuid,pk));

  new r:bitstring;
  let c:bitstring = aenc(pk,v,r) in
  let pi:bitstring = proofv(v,r,c,pk,pubs(sk_id),uuid) in
  let sigma:bitstring = Sign(sk_id,(c,pi,pubs(sk_id))) in
  let ballot:bitstring = ((c,pi,sigma),pubs(sk_id)) in
  event Voted(uuid,Voter,pubs(sk_id),v);
  out(cp,(ballot,SignAUTH(voter_password,ballot)));

  (* Process describing the voter's behavior when she verifies her voter *)
  in(cp,xCheck:bitstring);
  if xCheck = IsVerified then (
    (*in(cp,= OkReg(uuid,pubs(sk_id)));*) (* It is not necessary that the voter checks he receives a 'valid' credential *)

    (* The voter look for her ballot onto the bulletin board *)
    in(cellB(uuid,pubs(sk_id)),(i:nat,((=c,=pi,=sigma),=pubs(sk_id))));
    event VerifVoter(uuid,Voter,pubs(sk_id),(c,pi,sigma));
    out(cp,target_verif);
    (* The voter re-populates the cell *)
    out(cellB(uuid,pubs(sk_id)),(i,((c,pi,sigma),pubs(sk_id))));
    0
  ).

event UniqA(stamp,bitstring).
restriction st1,st2:stamp, vk:bitstring; event(UniqA(st1,vk)) && event(UniqA(st2,vk)) ==> st1 = st2.
let Voting_Server(dkVS:bitstring,uuid:bitstring) =
  (* Initialisation the global election key *)
    !out(cellPKvs(uuid), pubeMandatory(dkVS)) |
    in(cp,pkT:bitstring);
    !out(cellPKt, pkT) |
    let pk = pube(pubeMandatory(dkVS), pkT) in
    out(cp, pk);
    !out(cellPK(uuid),pk) |
  (* Publication of the public credential generate by the Registrar *)
  !(
    in(cp,vk:bitstring);
    new st:stamp; event UniqA(st,vk);
    out(cellVK(uuid),vk) |
    out(cellB(uuid,vk),(0,(initEmpty,vk))); (* Initialisation of the channel used to store the ballots of corresponding to this credential *)
    0
  ) |
  (* Main process of the Voting Server *)
  !(
    in(cp,voter_id:bitstring);
    in(cp,x_voter_password:bitstring); let voter_password = getPassword(uuid,voter_id,x_voter_password) in
    in(cp,(xm:bitstring,xsig:bitstring));
    if VerifyAUTH(pubs(voter_password),xm,xsig) then
    let ((c:bitstring,pi:bitstring,sigma:bitstring),vk:bitstring) = xm in

    in(cp, =OkReg(uuid,vk)); (* The Server accepts credentials confirmed by the Registrar only *)

    in(cellB(uuid,vk),(i:nat,(oldBallot:bitstring, =vk))); (* Get the previous ballot in the cell *)

    if
      Verify(vk,(c,pi,vk),sigma) &&
      verifproofv(pi,c,pk,vk,uuid)
    then (
      event In_log(uuid,voter_id,vk);
      event Accepted(uuid,voter_id, vk, (c,pi,sigma),i+1);
      event Rejected(uuid,voter_id, vk,oldBallot,i);
      out(cp,((c,pi,sigma),vk)); (* NOTE- We do not output 'j' since it is a nat which is "by definition" public; by the way, it avoids non-termination! *)
      out(cp,target_reject);
      out(cellB(uuid,vk),(i+1,((c,pi,sigma),vk))) |
      0
    )
  ). (* End main process Voting Server *)


(******************************)
(**** Recorded as intended ****)
(******************************)

(* Identity-based *)
query uuid:bitstring, id,id',ballot,v,vk,vk',vk'':bitstring, i,j:nat;
  event(Accepted(uuid,id, vk, ballot, j))  ==>
    ( event(Voted(uuid,id, vk, v)) && open_eq(v,ballot) )
    || event(Corrupted(uuid,id,vk'))
    [induction].

(*+ Credential-based *)
query uuid:bitstring, id,id',ballot,v,vk,vk',vk'':bitstring, i,j:nat;
  event(Accepted(uuid,id, vk, ballot, j))  ==>
    ( event(Registered(uuid,id',vk)) && event(Voted(uuid,id', vk, v)) && open_eq(v,ballot) )
    || event(Corrupted(uuid,id',vk))
    [induction].


(******************************)
(********* Eligibility ********)
(******************************)

(* Identity-based *)
query uuid:bitstring, id,ballot,vk,vk': bitstring, i,j:nat;
  event(Accepted(uuid,id,vk,ballot, j))
   ==> event(Registered(uuid,id,vk')) && (open_eq(ZERO,ballot) || open_eq(ONE,ballot))
    [induction].

(* Credential-based *)
query uuid:bitstring, id,id',ballot,vk,vk': bitstring, i,j:nat;
  event(Accepted(uuid,id,vk,ballot, j))
   ==> event(Registered(uuid,id',vk)) && (open_eq(ZERO,ballot) || open_eq(ONE,ballot))
    [induction].


(************************************)
(******* Individual verif (1) *******)
(************************************)
pred valid_zkp(bitstring,bitstring,bitstring,bitstring,bitstring).
clauses
  forall r:bitstring, pk:bitstring, vk:bitstring, uuid:bitstring;
      valid_zkp(proofv(ZERO,r,aenc(pk,ZERO,r),pk,vk,uuid),aenc(pk,ZERO,r),pk,vk,uuid);
  forall r:bitstring, pk:bitstring, vk:bitstring, uuid:bitstring;
      valid_zkp(proofv(ONE,r,aenc(pk,ONE,r),pk,vk,uuid),aenc(pk,ONE,r),pk,vk,uuid).

pred valid_sign(bitstring,bitstring,bitstring).
clauses forall Sk:bitstring, M:bitstring; valid_sign(pubs(Sk),M,Sign(Sk,M)).

pred valid_enc(bitstring,bitstring).
clauses forall PkM,PkT:bitstring, M:bitstring, R:bitstring; valid_enc(pube(PkM,PkT), aenc(pube(PkM,PkT),M,R)).

pred valid_ballot(bitstring,bitstring,bitstring,bitstring).
clauses forall uuid,pk,vk,c,pi,sigma:bitstring; valid_enc(pk,c) && valid_sign(vk,(c,pi,vk),sigma) && valid_zkp(pi,c,pk,vk,uuid) -> valid_ballot(uuid,pk,vk,(c,pi,sigma)).

(* Credential-based *)
query uuid:bitstring, id,id',ballot,vk,pk : bitstring, i:nat;
  event(VerifVoter(uuid,id,vk,ballot)) ==>
    event(Accepted(uuid,id',vk,ballot,i))
    && valid_ballot(uuid,pk,vk,ballot)
  [induction].

(************************************)
(******* Individual verif (2) *******)
(************************************)
pred open_eq_ballot(bitstring,bitstring).
clauses
  forall v:bitstring,pk,pk':bitstring,r1,r2:bitstring,pi1,pi2:bitstring,sigma1,sigma2:bitstring;
    open_eq_ballot((aenc(pk,v,r1),pi1,sigma1),(aenc(pk',v,r2),pi2,sigma2)).

query uuid,id,id',vk,ballot,ballot':bitstring, i,j:nat;
  event(Rejected(uuid,id,vk,ballot,i)) ==> (i = 0 || (event(Accepted(uuid,id',vk,ballot',j)) && j > i)) [induction].

query uuid, id,id',id'',vk,vk',ballot,ballot' : bitstring,i,i':nat;
  event(VerifVoter(uuid,id,vk,ballot)) && event(Accepted(uuid,id',vk,ballot',i))
    ==> ( open_eq_ballot(ballot,ballot') )
      || event(Corrupted(uuid,id,vk)) [induction].


(******************************)
(****** Extra properties ******)
(******************************)
pred open_eq(bitstring,bitstring).
clauses
  forall j:nat, v:bitstring,pk:bitstring,r:bitstring,pi:bitstring,sigma:bitstring; open_eq(v,(aenc(pk,v,r),pi,sigma)).

(* Check consistency *)
query uuid:bitstring, id,vk,v,ballot:bitstring;
  event(VerifVoter(uuid, id, vk, ballot)) ==> event(Voted(uuid,id, vk, v)) && open_eq(v,ballot) [induction].

(* Voter consistency *)
query uuid:bitstring, id,vk,vk',v,v':bitstring;
  event(Voted(uuid, id, vk, v)) && event(Voted(uuid,id, vk', v')) ==> vk = vk' [induction].

query uuid:bitstring,id,id',vk,vk',ballot,ballot':bitstring, i,j:nat;
  event(Accepted(uuid,id,vk,ballot,i)) && event(Accepted(uuid,id',vk',ballot',j)) ==> (id = id' && vk = vk') || (id <> id' && vk <> vk').

query uuid:bitstring,id,id',vk,vk':bitstring;
  event(Registered(uuid,id,vk)) && event(Registered(uuid,id',vk')) ==> (id = id' && vk = vk') || (id <> id' && vk <> vk').


query attacker(target_reject).
query attacker(target_verif).

process
  (*** Various processes used to make the protocol executable (e.g. initialising
  cells, removing parallel compositions or replications) ***)
  ( (*** Main process of the election ***)
    in(cp,dk1:bitstring); (* corrupted trustee *)
    in(cp,dk2:bitstring); (* corrupted trustee *)
    in(cp,dk3:bitstring); (* corrupted trustee *)
    (
      out(cp, pubeThreshold(dk1,dk2,dk3));

      in(cellPKt, =pubeThreshold(dk1,dk2,dk3));
      out(cp, OkTrustee(pubeThreshold(dk1,dk2,dk3)));
      0
    ) |
    !( (* beginning of a specific election *)
      new uuid:bitstring;
      out(cp, uuid);

      ( (* Process of the auditor *)
        in(cellPKt, pkT:bitstring);
        in(cp, =OkTrustee(pkT));
        in(cellPKvs(uuid), pkS:bitstring);
        let pk = pube(pkS,pkT) in
        in(cellPK(uuid), =pk);
        out(cp, OkPK(uuid,pk));
        0
      ) |

      new dkVS:bitstring;

      !( (* Generation of a voter *)
       in(cp, v_uniq:bitstring);
       new Voter:bitstring; out(cp, Voter);
       new sd:bitstring;
       new pw:bitstring; out(cp,pubs(pw));
       event Registered(uuid,Voter,pubs(privs(sd,uuid)));
       out(cp, seed(uuid,Voter,sd));
       out(cp, password(uuid,Voter,pw));
       May_corrupt_voter(Voter, sd, pw, uuid) |
       (
         (* The voter receives her personnal data (i.e. credentials and passwords) only once *)
         in(cp,personnal_data:bitstring) [precise];
         !(
           (* Application of the process Voter_Device *)
             (* in(cp,v:bitstring); *)
             Voter_Device(Voter, v_uniq, uuid, personnal_data)
         )
       )
      ) |
      !Registrar(uuid) |
      Voting_Server(dkVS,uuid)
    ) (* End beginning of an election *)
  ) (* End main process of the election *)
